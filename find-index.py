import time
import datetime
import requests
import json
import psycopg2
import os

class indexFinder():
#интервал времени для плавок для запроса от текущего момента
    def getDiapason(self):
        today=datetime.datetime.now().strftime( "%Y-%m-%d ")
        return str("2020-09-01 ")
#получение плавок
    def getHeatList(self):
        today=self.getDiapason()
        payload={"endDate":today+"23:59:59","ironSort": "mix","limeSort": "mix","mgSort": "mix","selectedUDCH": [1, 2],"sortament": "none","startDate": today+"00:00:00","team": "none"}
        headers={'Content-Type':'application/json;charset=UTF-8','Connection':'keep-alive','Cache-control':'no-cache','Authorization':'Bearer null','Accept-Encoding':'gzip,deflate,br','Accept':'*/*'}
        r=requests.post(os.environ.get('UDCH_HOST','http://localhost:3000/api/heat-list'),data=json.dumps(payload),headers=headers)
        if r.status_code == 200:
            data = json.loads(r.text)
            return data
        else:
            self.writeReport('  Не удалось подключиться к хосту для получения списка плавок')
            return False
#для каждой плавки получение и запись индекса с сервиса
    def getVideoIndexAndWrite(self,heats):
        start=0
        end=0
        for heat in heats:
            id_heat=heat['heat_no']
            aggregate_num=heat['process']['unit']
            start=heat['drain_times']['dateBeg']
            end=heat['drain_times']['dateEnd']
            index=self.getCameraIndex(num=aggregate_num, start=start,end=end)
            if index!=False:
                self.setIndex(index=str(index),id=str(id_heat))
            else: 
                self.writeReport('  Индекс плавки №'+str(id_heat)+' не получен')
#запись индекса в соответствующую таблицу бд 
    def setIndex(self,**kwargs):
        conn = psycopg2.connect(dbname=os.environ.get('DB_NAME','udch_test'), user=os.environ.get('DB_USER','postgres'), 
                        password=os.environ.get(DB_PASSWORD,'changeme_00dha'), host=os.environ.get('DB_HOST','172.18.0.2'), port=os.environ.get('DB_PORT','5432'))
        cursor=conn.cursor()
        cursor.execute('UPDATE raw_json SET video_index=%s where heat_no=%s',(kwargs['index'],kwargs['id']))
        cursor.close()
        conn.close()
#получение индекса 
    def getCameraIndex(self,**kwargs):
        camera_name=''
        result=False
        if kwargs['num'].find('1')!=-1:
            camera_name='1'
        else:
            camera_name='2'
        r = requests.get(os.environ.get('VIDEO_HOST','http://172.23.4.2:42002/api/metrics'), params={'dateBeg':kwargs['start'],'dateEnd':kwargs['end'], 'camera':camera_name})
        if r.status_code == 200:
            data = json.loads(r.text)
            if len(data) > 0:
                if 'Cycles' in data[0]:
                    if len(data[0]['Cycles']) > 0:
                        if len(data[0]['Cycles'][0]['Aggregate']) > 0:
                            index = data[0]['Cycles'][0]['Aggregate'][0]
                            if (len(index) > 0):
                                result = float(index[1].replace(',', '.'))
                                return result
        else:
            self.writeReport('  Ошибка запроса'+str(r.status_code))
            return False
        return False
#запись в отчет
    def writeReport(self,text):
            now=datetime.datetime.now()
            with open('./report/report.info' , 'a') as file:
                file.write('\n%s%s'%(now.strftime("%d-%m-%Y %H:%M"),text))      

def getHeats(self):
    conn = psycopg2.connect(dbname='udch', user='db_user', 
                        password='mypassword', host='localhost')
if __name__ == "__main__":
    f=indexFinder()
    try:
        heats=f.getHeatList()
        if heats!=False:
            f.getVideoIndexAndWrite(heats)
        else: 
            f.writeReport('  Плавки не были получены')
    except Exception as e:
        f.writeReport(' '+str(e))
