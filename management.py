from datetime import datetime
from threading import Thread
import schedule
import time
import subprocess
import os

class management():

    # def start(self):
    #     thread = Thread(target=self.jobThread,daemon=True)
    #     thread.start()
    def jobThread(self):
        schedule.every(1).hours.do(self.jobException)
        while True:
            schedule.run_pending()
            time.sleep(1)
    def jobException(self):
        try:
            self.writeReport("  >>  Старт процесса")
            print ("Старт процесса...")
            subprocess.check_output(['python','find-index.py'],timeout=300,stderr=subprocess.STDOUT, shell=True,universal_newlines=True)
            self.writeReport("  >>  Процесс завершен")
        except subprocess.TimeoutExpired as e:
            self.writeReport("  >>  Процесс завершился по таймауту:\n"+str(e))
            print ("Процесс завершился по таймауту: "+str(e))
        except subprocess.CalledProcessError as e:
            self.writeReport("  >>  Ошибка подпроцесса\n"+str(e.output))
            print("Ошибка в запущенном процессе "+str(e.output))
        except Exception as e:
            self.writeReport('  >>  Ошибка запуска процесса\n'+str(e))
    def writeReport(self,text):
        now=datetime.now()
        with open('./report/report.info' , 'a') as file:
            file.write('\n%s%s'%(now.strftime("%d-%m-%Y %H:%M"),text))
if __name__ == "__main__":
    mng=management()
    mng.jobThread()
