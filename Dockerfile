FROM python:3.7
COPY . .
ENV PIP_CONFIG_FILE pip.conf
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["python","-u","management.py"]